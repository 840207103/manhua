#-*- encoding: utf-8 -*-
import sqlite3

class sql(object):
    def __init__(self):
        # client = pymongo.MongoClient("localhost", 27017)
        # db = client.test
        conn = sqlite3.connect('manhua.db')
        conn.isolation_level = None #关闭事务
        self.cursor = conn.cursor()

    def query(self, sql):
        self.cursor.execute(sql)

    def select(self, sql):
        self.query(sql)
        return self.cursor.fetchall()

    def power(self,x, n=2):
        s = 1
        while n > 0:
            n = n - 1
            s = s * x
            return s

    def update(self,sql):
        self.query(sql)
        return self.cursor.rowcount

    def updateChapter(self, chapter ,id):
        self.cursor.execute('update manhua set chapter = ?  where id=?', (chapter,id))
        return self.cursor.rowcount
# 执行一条SQL语句，创建user表:
#cursor.execute('create table manhua (id varchar(20) primary key, name varchar(40) ,url varchar(40) ,web varchar(200),  type varchar(40) , chapter varchar(20))')
#cursor.execute('insert into manhua (id, name,url,web,type,chapter) values (\'1\', \'食戟之灵\',\'http://pan.baidu.com/pcloud/feed/getsharelist?category=0&auth_type=1&request_location=share_home&start=0&limit=100&query_uk=3057094636\',\'baidupan\',\'json\',\'1\')')
#cursor.rowcount
#cursor.close()
#conn.commit()
#conn.close()