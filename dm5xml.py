#-*- encoding: utf-8 -*-
import xml.sax
import re
import copy

class MovieHandler(xml.sax.ContentHandler):
   def __init__(self):
      self.CurrentData = ""
      self.type = ""
      self.format = ""
      self.title = ""
      self.stars = ""
      self.link = ""
      self.description = ""
      self.maxchapter= 0
      self.info1 = list()
      self.info = dict()

   # 元素开始事件处理
   def startElement(self, tag, attributes):
      self.CurrentData = tag


   # 元素结束事件处理
   def endElement(self, tag):
      if self.CurrentData == "title":
         self.info['title'] = self.title
         match = re.search(r'(\d+)', self.title)
         if(match):
            self.info['chapter'] = int(match.group(0))
            if(self.info['chapter'] > self.maxchapter):
               self.maxchapter = self.info['chapter']
         else:
            self.info['chapter'] = ''
      if self.CurrentData == "link":
         self.info['url'] = self.link
         #print(self.info)
      if self.CurrentData == "":
         self.info1.append(copy.deepcopy(self.info))
         self.info.clear()
      self.CurrentData = ""

   # 内容事件处理
   def characters(self, content):
      if self.CurrentData == "title":
         self.title = content
      if self.CurrentData == "link":
         self.link = content

   def start(self, url):
      Handler = MovieHandler()
      parser = xml.sax.parseString(url, Handler)
      dict1 =dict()
      dict1['info'] = Handler.info1
      dict1['type'] = 'down'
      dict1['maxchapter'] = Handler.maxchapter
      return dict1