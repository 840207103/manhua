#-*- encoding: utf-8 -*-
import json
import re

def cross(type, stream, list):
    if (type == 'baidupan'):
        result = baidupan(stream,list)
    elif (type == 'dm5'):
        result = dm5(stream,list)
    elif (type == 'wangyi163'):
        result = manhua163(stream,list)
    elif (type == 'dmzj'):
        result = dmzj(stream,list)
    if(result['maxchapter']>int(list['chapter'])):
        result['update'] = True
        result['url'] = list['RefererUrl']
    else:
        result['update'] = False
        result['url'] = list['RefererUrl']
    return result


def baidupan(stream,maninfo):
    date = json.loads(stream)
    dict = {}
    dict['info'] = list()
    maxchapter = 0
    for i,n in enumerate(date['records']):
        if(n['title'].find(maninfo['name'])>=0):
            info = {}
            match = re.search(r'(\d+)', n['title'])
            if(match):
                info['chapter'] = int(match.group(0))
                if(info['chapter'] > maxchapter):
                    maxchapter = info['chapter']
            else:
                info['chapter'] = ''
            info['title'] = n['title']
            info['url'] = n['shorturl']
            info['item'] = ''
            dict['info'].append(info)
    dict['id'] = maninfo['id']
    dict['name'] = maninfo['name']
    dict['type'] = 'down'
    dict['maxchapter'] = maxchapter
    return dict

def manhua163(stream,maninfo):
    date = json.loads(stream)
    dict = {}
    dict['info'] = list()
    maxchapter = 0
    for i,n in enumerate(date['catalog']['sections'][0]['sections']):
        info = {}
        match = re.search(r'(\d+)', n['titleOrder'])
        if(match):
            info['chapter'] = int(match.group(0))
            if(info['chapter'] > maxchapter):
                maxchapter = info['chapter']
        else:
            info['chapter'] = ''
        info['title'] = n['title']
        info['url'] = 'http://manhua.163.com/reader/'+n['bookId']+'/'+n['sectionId']
        info['item'] = ''
        dict['info'].append(info)
    dict['id'] = maninfo['id']
    dict['name'] = maninfo['name']
    dict['type'] = 'view'
    dict['maxchapter'] = maxchapter
    return dict

def dmzj(stream,maninfo):
    import dmzjxml
    handler =dmzjxml.MovieHandler()
    dict = handler.start(stream)
    dict['name'] = maninfo['name']
    dict['id'] = maninfo['id']
    return dict

def dm5(stream,maninfo):
    import dm5xml
    handler =dm5xml.MovieHandler()
    dict = handler.start(stream)
    dict['name'] = maninfo['name']
    dict['id'] = maninfo['id']
    return dict