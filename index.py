#-*- encoding: utf-8 -*-
import sqlite
import jsonAnalysis
import re
import smtp
from urllib import request

def istype(list, type):
    list['RefererUrl'] = url = list['url']
    if (type == 'baidupan'):
        uk = re.search(r'uk=[1-9]\d*', url).group(0).replace('uk=','')
        list['RequestUrl'] = 'http://pan.baidu.com/pcloud/feed/getsharelist?category=0&auth_type=1&request_location=share_home&start=0&limit=100&query_uk='+uk
    elif (type == 'dm5'):
        list['RequestUrl'] = url.replace('manhua-','rss-')
    elif (type == 'wangyi163'):
        uk = re.search(r'/[1-9]\d*', url).group(0).replace('/','')
        list['RequestUrl'] = 'http://manhua.163.com/book/catalog/'+uk
    elif (type == 'dmzj'):
        uk = url.replace('http://manhua.dmzj.com/','').replace('/','')
        list['RequestUrl'] = 'http://manhua.dmzj.com/'+uk[0:1]+'/'+ uk +'/rss.xml'
    else:
        return ''
    stream = urllib(list)
    result = jsonAnalysis.cross(type,stream, list)
    return result

def urllib(info):
    req = request.Request(info['RequestUrl'])
    req.add_header('User-Agent','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36)')
    req.add_header('Referer', info['RefererUrl'])
    f = request.urlopen(req)
    return f.read().decode('utf-8')

db = sqlite.sql()
sql = 'select * FROM manhua where 1'
mob = db.select(sql)
info = dict()
result = result1 = list()
key = ['id','name','url','web','type','chapter','datatype','is_down']
for index,i in enumerate(mob):
    info[index] = dict(zip(key,i))
    result = istype(info[index],info[index]['web'])
    print(result)
    if(result['update']):
        sql = "update manhua SET chapter = "+str(result['maxchapter'])+" where id = "+str(result['id'])
        db.update(sql)
        result1.append(result)
if(result1):
    smtp.html(result1)


# 'http://pan.baidu.com/pcloud/feed/getsharelist?category=0&auth_type=1&request_location=share_home&start=0&limit=100&query_uk=3057094636' #百度盘
# 'http://manhua.dmzj.com/m/magi/rss.xml'# magi rss
# 'http://www.dm5.com/rss-fuxiaodeyouna/' # 晨曦公主 rss
# 'http://manhua.163.com/book/catalog/4458002705630123021?_c=1465748711006' #网易 斩赤红之瞳